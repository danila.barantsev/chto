function setblock(p) {
    function updatePrice() {

        let s = document.getElementsByName("prodType-" + p);
        let select = s[0];
        let textarea = Number(document.getElementById("kol-" + p).value);
        let price = Number(document.getElementById("constPrice-" + p).textContent);
        let prices = getPrices();
        let priceIndex = parseInt(select.value) - 1;
        if (priceIndex >= 0) {
            if (prices.prodTypes[priceIndex] != 0) {
                price = (price + prices.prodTypes[priceIndex]) * textarea;
            } else {
                price *= textarea;
            };
        }


        let radioDiv = document.getElementById("radios-" + p);
        radioDiv.style.display = (select.value == "1" || select.value == "3" ? "none" : "block");

        let MyElementName = document.getElementById("radios-" + p);
        if (MyElementName.style.display != "none") {
            let radios = document.getElementsByName("prodOptions-" + p);
            radios.forEach(function (radio) {
                if (radio.checked) {
                    let optionPrice = prices.prodOptions[radio.value];
                    if (optionPrice !== undefined) {
                        price += optionPrice;
                    }
                }
            })
        };


        let checkDiv = document.getElementById("checkboxes-" + p);
        checkDiv.style.display = (select.value == "1" || select.value == "2" ? "none" : "block");


        let MyElement = document.getElementById("checkboxes-" + p);
        if (MyElement.style.display != "none") {
            let check = document.querySelector("#checkboxes-" + p);
            let checkboxes = check.querySelectorAll("input");
            checkboxes.forEach(function (checkbox) {
                if (checkbox.checked) {
                    let propPrice = prices.prodProperties[checkbox.name];
                    if (propPrice !== undefined) {
                        price += textarea * propPrice;
                    }
                }
            })
        };

        let prodPrice = document.getElementById("prodPrice-" + p);
        prodPrice.innerHTML = price + "₽";
    }

    function getPrices() {
        return {
            prodTypes: [0, 3000, 5000],
            prodOptions: {
                option1: 400,
                option2: 600,
                option3: 800,
            },
            prodProperties: {
                prop1: 2500,
                prop2: 1500,
            }
        };
    }

    window.addEventListener('DOMContentLoaded', function (event) {

        let radioDiv = document.getElementById("radios-" + p);
        radioDiv.style.display = "none";


        let s = document.getElementsByName("prodType-" + p);
        let select = s[0];

        select.addEventListener("change", function (event) {
            let target = event.target;
            console.log(target.value);
            updatePrice();
        });
        let textarea = document.getElementById("kol-" + p);
        textarea.addEventListener("change", function (event) {
            let f = event.target;
            console.log(f.value);
            updatePrice();
        });

        let radios = document.getElementsByName("prodOptions-" + p);
        radios.forEach(function (radio) {
            radio.addEventListener("change", function (event) {
                let r = event.target;
                console.log(r.value);
                updatePrice();
            });
        });
        select.addEventListener("change", function (event) {
            let target = event.target;
            console.log(target.value);
            updatePrice();
        });

        let check = document.querySelector("#checkboxes-" + p);
        console.log(check);
        let checkboxes = check.querySelectorAll("input");
        console.log(checkboxes);
        checkboxes.forEach(function (checkbox) {
            checkbox.addEventListener("change", function (event) {
                let c = event.target;
                console.log(c.name);
                console.log(c.value);
                updatePrice();
            });
        });
        updatePrice();
    })
};

function setBLocks() {
    var inputs = document.getElementById("adress");
    var input = inputs.children[0].childNodes[1].childNodes[1].childNodes[1].children;
    console.log(input.length);
    for (var i = 0; i < input.length; i++) {
        var pol = input[i].children[0].childNodes[7];
        console.log(pol);
        var l = String(i);
        setblock(l);
    }
}
setBLocks();

$('.ssslideee').slick({
    infinite: true,
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    variableWidth: false,
    responsive: [
  {
      breakpoint: 496,
      settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          centerMode: false,
          centerPadding: '8px',
      }
  }
    ]
});